package com.dichuk.tests.domain

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class Hotel(val id: Int, val name: String, val address: String, val stars: Double,
                 val distance: Double, @SerializedName("suites_availability") val suitesAvailability: String,
                 @Expose val image: String,
                 @Expose val lat: Double,
                 @Expose val lon: Double) : Comparable<Hotel> {

    override fun compareTo(other: Hotel): Int {
        return if (distance >= other.distance) {
            1
        } else {
            -1
        }
    }
}
