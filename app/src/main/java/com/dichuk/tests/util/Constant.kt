package com.dichuk.tests.util

class Constant {
    companion object {
        const val BASE_URL = "https://bitbucket.org/dichuk_dev/temps/raw/"
        const val BASE_IMAGE_URL = "${BASE_URL}6dcdf8a2477ab350cd956a0c4a38e399e41296bf/data/"
    }
}