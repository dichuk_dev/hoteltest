package com.dichuk.tests.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import com.dichuk.tests.R
import com.dichuk.tests.adapter.core.BaseRecyclerViewAdapter
import com.dichuk.tests.adapter.core.BaseViewWrapper
import com.dichuk.tests.adapter.hotel.HotelViewHolder
import com.dichuk.tests.domain.Hotel

class HotelAdapter : BaseRecyclerViewAdapter<Hotel, HotelViewHolder>() {

    override fun onCreateItemView(parent: ViewGroup, viewType: Int): HotelViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
                R.layout.view_holder_hotel, parent, false)
        return HotelViewHolder(view, clickListener)
    }

    override fun onBindViewHolder(viewHolder: BaseViewWrapper<HotelViewHolder>, position: Int) {
        val holder = viewHolder.getView()
        val item = getItems()[position]
        holder.bind(item, position)
    }
}


