package com.dichuk.tests.adapter.hotel

import android.support.constraint.ConstraintLayout
import android.view.View
import android.widget.TextView
import com.dichuk.tests.R
import com.dichuk.tests.adapter.core.BaseViewHolder
import com.dichuk.tests.adapter.core.OnItemClickListener
import com.dichuk.tests.domain.Hotel

class HotelViewHolder(view: View, onItemClickListener: OnItemClickListener<Hotel>) :
        BaseViewHolder<Hotel>(view, onItemClickListener) {

    private lateinit var header: ConstraintLayout
    private lateinit var tvName: TextView
    private lateinit var tvAddress: TextView

    private lateinit var hotel: Hotel

    override fun initUI() {
        header = findViewById(R.id.cl_header)
        tvName = findViewById(R.id.tv_name)
        tvAddress = findViewById(R.id.tv_address)
    }

    override fun setListener() {
        header.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        when (view.id) {
            R.id.cl_header -> {
                onItemClickListener.onItemClick(hotel, 0)
            }
        }
    }

    override fun bind(hotel: Hotel, position: Int) {
        this.hotel = hotel
        tvName.text = hotel.name
        tvAddress.text = "Address: ${hotel.address}"
    }

}