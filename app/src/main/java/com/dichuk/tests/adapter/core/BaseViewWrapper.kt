package com.dichuk.tests.adapter.core

import android.support.v7.widget.RecyclerView
import android.view.View

class BaseViewWrapper<V : View>(private val view: V) : RecyclerView.ViewHolder(view) {

    fun getView(): V {
        return view
    }
}