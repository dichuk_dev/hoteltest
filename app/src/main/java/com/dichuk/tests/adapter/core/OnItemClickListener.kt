package com.dichuk.tests.adapter.core

interface OnItemClickListener<T> {
    fun onItemClick(t: T, position: Int)
}