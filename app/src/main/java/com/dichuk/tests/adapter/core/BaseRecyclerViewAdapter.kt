package com.dichuk.tests.adapter.core

import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup

abstract class BaseRecyclerViewAdapter<T, V : View> : RecyclerView.Adapter<BaseViewWrapper<V>>() {

    private var items: List<T> = ArrayList()

    protected lateinit var clickListener: OnItemClickListener<T>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewWrapper<V> {
        return BaseViewWrapper(onCreateItemView(parent, viewType))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun setItems(items: List<T>) {
        this.items = items
        this.notifyDataSetChanged()
    }

    fun setOnItemClickListener(clickListener: OnItemClickListener<T>) {
        this.clickListener = clickListener
    }

    fun getItems(): List<T> {
        return items
    }

    protected abstract fun onCreateItemView(parent: ViewGroup, viewType: Int): V
}