package com.dichuk.tests.api

import com.dichuk.tests.domain.Hotel
import com.dichuk.tests.util.Constant
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ServiceInterface {


    @GET("6dcdf8a2477ab350cd956a0c4a38e399e41296bf/data/0777.json")
    fun getHotelList(): Observable<List<Hotel>>

    @GET("6dcdf8a2477ab350cd956a0c4a38e399e41296bf/data/{id}.json")
    fun getHotel(@Path("id") id: Int): Observable<Hotel>

    companion object Factory {
        fun create(): ServiceInterface {
            val retrofit = Retrofit.Builder()
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .baseUrl(Constant.BASE_URL)
                    .build()

            return retrofit.create(ServiceInterface::class.java)
        }
    }
}
