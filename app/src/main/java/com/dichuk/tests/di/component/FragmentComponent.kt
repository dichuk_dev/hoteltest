package com.dichuk.tests.di.component

import com.dichuk.tests.di.module.FragmentModule
import com.dichuk.tests.fragment.DashboardFragment
import com.dichuk.tests.fragment.HotelDetailFragment
import dagger.Component


@Component(modules = [FragmentModule::class])
interface FragmentComponent {
    fun inject(dashboardFragment: DashboardFragment)

    fun inject(hotelDetailFragment: HotelDetailFragment)
}