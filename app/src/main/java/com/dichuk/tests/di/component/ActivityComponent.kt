package com.dichuk.tests.di.component

import com.dichuk.tests.MainActivity
import com.dichuk.tests.di.module.ActivityModule
import dagger.Component

@Component(modules = [ActivityModule::class])
interface ActivityComponent {
    fun inject(app: MainActivity)
}