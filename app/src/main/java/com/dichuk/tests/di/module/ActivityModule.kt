package com.dichuk.tests.di.module

import android.support.v7.app.AppCompatActivity
import com.dichuk.tests.MainActivity
import com.dichuk.tests.mvp.Main
import com.dichuk.tests.mvp.ui.ActivityPresenter
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ActivityModule(val activity: MainActivity) {

    @Provides
    @Singleton
    fun provideActivity(): AppCompatActivity = activity

    @Provides
    fun providePresenter(): Main.Presenter = ActivityPresenter()

}
