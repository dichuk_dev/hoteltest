package com.dichuk.tests.di.module

import com.dichuk.tests.api.ServiceInterface
import com.dichuk.tests.mvp.HotelView
import com.dichuk.tests.mvp.ui.HotelPresenter
import dagger.Module
import dagger.Provides

@Module
class FragmentModule {

    @Provides
    fun provideServiceApi(): ServiceInterface = ServiceInterface.create()

    @Provides
    fun provideDashboard(): HotelView.Presenter = HotelPresenter()

}