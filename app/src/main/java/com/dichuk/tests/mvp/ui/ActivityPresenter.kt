package com.dichuk.tests.mvp.ui

import com.dichuk.tests.mvp.Main

class ActivityPresenter : Main.Presenter {

    private lateinit var view: Main.View


    override fun initView(view: Main.View) {
        this.view = view
        view.showDashboardFragment()
    }

    override fun openDetailHotel(hotelID: Int) {
        view.showHotelDetailFragment(hotelID)
    }
}