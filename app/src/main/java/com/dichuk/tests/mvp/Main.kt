package com.dichuk.tests.mvp

import com.dichuk.tests.mvp.core.Base

class Main {

    interface View : Base.View {
        fun showDashboardFragment()
        fun showHotelDetailFragment(hotelID: Int)
    }

    interface Presenter : Base.Presenter<Main.View> {
        fun openDetailHotel(hotelID: Int)

    }
}