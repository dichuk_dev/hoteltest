package com.dichuk.tests.mvp.ui

import com.dichuk.tests.api.ServiceInterface
import com.dichuk.tests.domain.Hotel
import com.dichuk.tests.mvp.HotelView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers


class HotelPresenter : HotelView.Presenter {

    private lateinit var view: HotelView.View

    override fun initView(view: HotelView.View) {
        this.view = view
    }

    private val api: ServiceInterface = ServiceInterface.create()


    override fun loadData(id: Int) {
        val sub = api.getHotel(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result: Hotel ->
                    view.onProgress(false)
                    view.onSuccessData(result)
                }, { error ->
                    view.onProgress(false)
                })
    }


    override fun loadDataAll() {
        val sub = api.getHotelList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ result: List<Hotel> ->
                    view.onProgress(false)
                    view.onSuccessData(result)
                }, { error ->
                    view.onProgress(false)
                })
    }

}