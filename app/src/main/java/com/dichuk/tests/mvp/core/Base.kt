package com.dichuk.tests.mvp.core

open class Base {

    interface Presenter<in T> {
        fun initView(view: T)
    }

    interface View
}