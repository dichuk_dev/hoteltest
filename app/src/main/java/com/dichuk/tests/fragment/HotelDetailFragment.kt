package com.dichuk.tests.fragment

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.dichuk.tests.R
import com.dichuk.tests.di.component.DaggerFragmentComponent
import com.dichuk.tests.di.module.FragmentModule
import com.dichuk.tests.domain.Hotel
import com.dichuk.tests.fragment.core.BaseFragment
import com.dichuk.tests.mvp.HotelView
import com.dichuk.tests.util.Constant.Companion.BASE_IMAGE_URL
import com.dichuk.tests.util.CropTransformation
import com.squareup.picasso.Picasso
import javax.inject.Inject
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.MarkerOptions


class HotelDetailFragment : BaseFragment(), HotelView.View, OnMapReadyCallback {

    @Inject
    lateinit var presenter: HotelView.Presenter

    private lateinit var pbIndicator: ProgressBar
    private lateinit var ivHotel: ImageView
    private lateinit var tvName: TextView
    private lateinit var tvAddress: TextView
    private lateinit var tvStars: TextView
    private lateinit var tvDistance: TextView
    private lateinit var tvSuitesAva: TextView
    private lateinit var googleMap: GoogleMap

    private var hotelID = 0

    private lateinit var hotel: Hotel

    fun newInstance(hotelID: Int): BaseFragment {
        val fragment = HotelDetailFragment()
        val args = Bundle()
        args.putInt("hotelID", hotelID)
        fragment.arguments = args
        return fragment
    }

    override fun restoreBundle() {
        hotelID = arguments?.getInt("hotelID")!!
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragemnt_detail_hotel
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
    }


    override fun registerPresenter() {
        presenter.initView(this)
        presenter.loadData(hotelID)
    }

    override fun initUI(baseView: View) {
        pbIndicator = baseView.findViewById(R.id.pb_indicator)
        ivHotel = baseView.findViewById(R.id.iv_hotel)
        tvName = baseView.findViewById(R.id.tv_name)
        tvAddress = baseView.findViewById(R.id.tv_address)
        tvStars = baseView.findViewById(R.id.tv_stars)
        tvDistance = baseView.findViewById(R.id.tv_distance)
        tvSuitesAva = baseView.findViewById(R.id.tv_suites_avail)
    }

    private fun setData(hotel: Hotel) {
        this.hotel = hotel
        Picasso.get()
                .load(BASE_IMAGE_URL + hotel.image)
                .transform(CropTransformation(1))
                .into(ivHotel)

        tvName.text = hotel.name
        tvAddress.text = "Address: ${hotel.address}"
        tvStars.text = "Stars: ${hotel.stars}"
        tvDistance.text = "Distance: ${hotel.distance}"
        tvSuitesAva.text = "Apartment number: ${hotel.suitesAvailability}"

        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onProgress(isProgress: Boolean) {
        if (isProgress) {
            pbIndicator.visibility = View.VISIBLE
        } else {
            pbIndicator.visibility = View.GONE
        }
    }

    override fun onSuccessData(hotel: Hotel) {
        setData(hotel)

    }

    override fun onSuccessData(list: List<Hotel>) {
    }


    override fun onMapReady(googleMap: GoogleMap?) {
        this.googleMap = googleMap!!
        drawMarket()
        settingMap()
    }

    private fun drawMarket() {
        googleMap.addMarker(MarkerOptions()
                .position(LatLng(hotel.lat, hotel.lon))
                .title(hotel.name)
                .snippet("Address : ${hotel.address}"))

    }

    private fun settingMap() {
        val cameraPosition = CameraPosition.builder()
                .target(LatLng(hotel.lat, hotel.lon))
                .zoom(10f)
                .bearing(0f)
                .tilt(45f)
                .build()
        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 3000, null)
    }


    private fun inject() {
        val component = DaggerFragmentComponent.builder()
                .fragmentModule(FragmentModule())
                .build()
        component.inject(this)
    }

}