package com.dichuk.tests.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.widget.ProgressBar
import com.dichuk.tests.R
import com.dichuk.tests.adapter.HotelAdapter
import com.dichuk.tests.adapter.core.OnItemClickListener
import com.dichuk.tests.di.component.DaggerFragmentComponent
import com.dichuk.tests.di.module.FragmentModule
import com.dichuk.tests.domain.Hotel
import com.dichuk.tests.fragment.core.BaseFragment
import com.dichuk.tests.mvp.HotelView
import com.dichuk.tests.util.adapter.ItemDecoration
import javax.inject.Inject

class DashboardFragment : BaseFragment(), HotelView.View {

    @Inject
    lateinit var presenter: HotelView.Presenter

    private lateinit var pbIndicator: ProgressBar
    private lateinit var rvHotel: RecyclerView

    private lateinit var hotelAdapter: HotelAdapter

    private lateinit var hotelList: List<Hotel>

    private var isSort = true

    override fun newInstance(): BaseFragment {
        return DashboardFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        inject()
    }

    override fun getLayoutResource(): Int {
        return R.layout.fragment_dashboard
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater!!.inflate(R.menu.sort, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return (when (item!!.itemId) {
            R.id.menu_sort_distance -> {
                if (isSort) {
                    val sortedHotel = hotelList.sorted()
                    hotelAdapter.setItems(sortedHotel)
                    isSort = false
                } else {
                    hotelAdapter.setItems(hotelList)
                    isSort = true
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        })
    }

    override fun registerPresenter() {
        presenter.initView(this)
        presenter.loadDataAll()
    }

    override fun initUI(baseView: View) {
        setHasOptionsMenu(true)
        pbIndicator = baseView.findViewById(R.id.pb_indicator)
        rvHotel = baseView.findViewById(R.id.rv_hotel)
        initAdapter()
    }

    private fun initAdapter() {
        hotelAdapter = HotelAdapter()
        hotelAdapter.setOnItemClickListener(object : OnItemClickListener<Hotel> {
            override fun onItemClick(hotel: Hotel, position: Int) {
                isSort = true
                getMainActivity().getMainPresenter().openDetailHotel(hotel.id)
            }
        })
        rvHotel.layoutManager = LinearLayoutManager(activity)
        rvHotel.addItemDecoration(ItemDecoration(R.dimen.item_margin))
        rvHotel.adapter = hotelAdapter
    }

    override fun onProgress(isProgress: Boolean) {
        if (isProgress) {
            pbIndicator.visibility = View.VISIBLE
        } else {
            pbIndicator.visibility = View.GONE
        }
    }

    override fun onSuccessData(list: List<Hotel>) {
        hotelList = list
        hotelAdapter.setItems(list)
    }

    override fun onSuccessData(hotel: Hotel) {

    }

    private fun inject() {
        val component = DaggerFragmentComponent.builder()
                .fragmentModule(FragmentModule())
                .build()
        component.inject(this)
    }


}